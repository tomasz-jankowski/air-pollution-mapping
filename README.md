<div align="center">

  <img src="https://gitlab.com/tomasz-jankowski/air-pollution-mapping/uploads/1e0d92eb729abebb40fc9ec605ef591a/85040383-0920c580-b189-11ea-9945-e6f76bc0fbe4.png" alt="Measuring station" width="500" />

  <h1>Air pollution mapping</h1>

  <a href="https://nodejs.org/"><img alt="Node.js" src="https://img.shields.io/badge/node.js-V12.13.1-%23404d59.svg?logo=node.js&logoColor=white" height="20"/></a>
  <a href="https://expressjs.com/"><img alt="Express.js" src="https://img.shields.io/badge/express.js-%23404d59.svg?logo=express&logoColor=%2361DAFB" height="20"/></a>
  <a href="https://www.mongodb.com/"><img alt="MongoDB" src="https://img.shields.io/badge/mongodb-%23404d59.svg?logo=mongodb&logoColor=white" height="20"/></a>

  <p><b>Monitoring and mapping air pollution reported by drone-driven measurement stations.</b></p>

  </br>

  <sub>Web application used to gather real-time air quality measurements from drone-driven measuring station. Gathered data is visualized on a map and can be filtered by date, time and type of measurement. Part of BSc thesis.<sub>

</div>


![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Installation

```
$ npm install
```

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Configuration

* Provide `MongoDB` connection string within `app.js`.
* Set `PORT` environment variable, default port: 3000.

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Usage

```
$ npm start
```

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Data frame

Measuring station sends data in specific format, as shown below. Key `gps` is in NMEA 0183 format, as reported by the GPS module used; `hcho` is float and the rest are integers.

```json
{
    "gps": "$GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47",
    "pm1": 10,
    "pm25": 20,
    "pm4": 30,
    "pm10": 40,
    "so2": 50,
    "hcho": 1.23
}
```

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Pollution map (dummy data)

Uses `Google Maps JavaScript API V3`.
<p align="center">
  <img src="https://gitlab.com/tomasz-jankowski/air-pollution-mapping/uploads/54d5cdfa63d889f17298fe520312c206/85042274-4d14ca00-b18b-11ea-98ea-f91e6aeddccc.png" alt="Measuring station" />
</p>

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Afterthoughts

Project, being the first I developed, suffers from many bad practices, redundant and messy code. Issues and improvements I would implement now:

* `MongoDB` connection string should be populated from environment variables.
* `PORT` variable is not reflected in initial startup log message.
* Application is not secured from unauthorized requests (limitation of used hardware).
* Some of the views are missing (`latex`, `logs`).
* Object destructuring assignments could be added.


![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Credit
As the application being a part of BSc thesis, data shape was consulted with hardware co-creators:
* [Michał Kliczkowski](https://github.com/michal090497)
* [Wojciech Stróżecki](https://github.com/iamWojtas)

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## License

Licensed under [MIT](https://opensource.org/license/mit/).
